import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ExpirmentService } from '../../services/expirment.service';

@Component({
  selector: 'app-expirment',
  templateUrl: './expirment.component.html',
  styleUrls: ['./expirment.component.scss']
})
export class ExpirmentComponent implements OnInit {
  expirmentForm!: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private expirmentService: ExpirmentService,
    private router:Router
  ) {}

  ngOnInit(): void {
    this.initForm();
  }
  get formControls() {
    return this.expirmentForm.controls;
  }
  initForm() {
    this.expirmentForm = this.formBuilder.group({
      name: ['enter expirment name', Validators.required],
      description: ['enter expirment description', Validators.required],
    });
  }
  sendData() {
    this.expirmentService.createExpirment(this.expirmentForm.value);
    this.expirmentForm.reset()
    this.router.navigate(['auth-tool'])
  }
}


