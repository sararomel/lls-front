import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ExpirmentService {

  constructor(private http: HttpClient) {}
  // Post method
  postRequest(url: string, body: {}): Observable<any> {
    return this.http.post<any>(environment.baseUrl + url, body);
  }
  createExpirment(body: {}) {
    this.postRequest('/api/Experiment/Create', body).subscribe({
      next: (res: any) => {
        console.log('resexp',res);
        
      },
      error: (error: any) => {},
    });
  }}
