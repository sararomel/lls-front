import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { ExpirmentEffects } from './expirment.effects';

describe('ExpirmentEffects', () => {
  let actions$: Observable<any>;
  let effects: ExpirmentEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ExpirmentEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.inject(ExpirmentEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
