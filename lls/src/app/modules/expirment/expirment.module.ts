import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExpirmentRoutingModule } from './expirment-routing.module';
import { ExpirmentComponent } from './pages/expirment/expirment.component';


@NgModule({
  declarations: [
    ExpirmentComponent
  ],
  imports: [
    CommonModule,
    ExpirmentRoutingModule
  ]
})
export class ExpirmentModule { }
