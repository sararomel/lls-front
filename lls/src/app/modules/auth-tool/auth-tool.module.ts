import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthToolRoutingModule } from './auth-tool-routing.module';
import { AuthToolComponent } from './pages/auth-tool/auth-tool.component';
// import { DragDropModule } from 'primeng/dragdrop';
// import { PanelModule } from 'primeng/panel';

@NgModule({
  declarations: [AuthToolComponent],
  imports: [CommonModule, AuthToolRoutingModule],
})
export class AuthToolModule {}
