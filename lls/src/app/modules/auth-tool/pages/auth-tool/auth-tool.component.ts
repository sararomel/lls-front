import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-auth-tool',
  templateUrl: './auth-tool.component.html',
  styleUrls: ['./auth-tool.component.scss']
})
export class AuthToolComponent implements OnInit {


  constructor(private fb: FormBuilder) {}
  ngOnInit(): void {
  }
  colors = ['question one', 'green', 'white'];

  droped = [];
  formsMsq!: FormGroup;
  dragedColor = null;

  dragStart(e: any, c: any) {
    this.dragedColor = c;
    this.initForm();
    console.log('ssssssss');
    
  }

  dragEnd(e: any) {}

  drop(e: any) {
    console.log('enter in drop');
    
    if (this.dragedColor) {
      this.droped.push(this.dragedColor);
      this.dragedColor = null;
    }
  }
  initForm() {
    this.formsMsq = this.fb.group({
      question: new FormControl(['please write your question?']),
      answerO: new FormControl(['first answer']),
      ansewrT: new FormControl(['second answer']),
    });
  }
}
