import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './core/auth/components/login/login.component';
import { AuthToolComponent } from './modules/auth-tool/pages/auth-tool/auth-tool.component';
import { ExpirmentComponent } from './modules/expirment/pages/expirment/expirment.component';
import { HomeComponent } from './modules/home/pages/home/home.component';

const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'expirment',component:ExpirmentComponent},
  {path:'auth-tool',component:AuthToolComponent},
  {path:'login',component:LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
