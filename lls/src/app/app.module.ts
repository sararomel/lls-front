import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './core/auth/components/login/login.component';
import { SignupComponent } from './core/auth/components/signup/signup.component';
import { SendCodeComponent } from './core/auth/components/send-code/send-code.component';
import { ForgetPasswordComponent } from './core/auth/components/forget-password/forget-password.component';
import { VerifyCheckComponent } from './core/auth/components/verify-check/verify-check.component';
import { VerifyFailComponent } from './core/auth/components/verify-fail/verify-fail.component';
import { VerifySuccessComponent } from './core/auth/components/verify-success/verify-success.component';
import { LoaderComponent } from './core/components/loader/loader.component';
import { FooterComponent } from './core/components/main-layout/footer/footer.component';
import { HeaderComponent } from './core/components/main-layout/header/header.component';
import { LayoutComponent } from './core/components/main-layout/layout/layout.component';
import { FormsInputFormsSpinnerComponent } from './shared/components/forms-input-forms-spinner/forms-input-forms-spinner.component';
import { DescPipe } from './shared/pipes/desc.pipe';
import { SubPipe } from './shared/pipes/sub.pipe';
import { HomeComponent } from './modules/home/pages/home/home.component';
import { ExpirmentComponent } from './modules/expirment/pages/expirment/expirment.component';
import { AuthToolComponent } from './modules/auth-tool/pages/auth-tool/auth-tool.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Interceptors } from './core/services/interceptors.service';
import { CommonModule } from '@angular/common';
import {ButtonModule} from 'primeng/button';
// import { AuthToolModule } from './modules/auth-tool/auth-tool.module';
import { DragDropModule } from 'primeng/dragdrop';
import { PanelModule } from 'primeng/panel';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    ExpirmentComponent,
    AuthToolComponent,
    SignupComponent,
    SendCodeComponent,
    ForgetPasswordComponent,
    VerifyCheckComponent,
    VerifyFailComponent,
    VerifySuccessComponent,
    LoaderComponent,
    FooterComponent,
    HeaderComponent,
    LayoutComponent,
    FormsInputFormsSpinnerComponent,
    DescPipe,
    SubPipe,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    ButtonModule,
    DragDropModule,
    // AuthToolModule
    PanelModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptors,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
