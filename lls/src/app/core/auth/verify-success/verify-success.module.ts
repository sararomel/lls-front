import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VerifySuccessRoutingModule } from './verify-success-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    VerifySuccessRoutingModule
  ]
})
export class VerifySuccessModule { }
