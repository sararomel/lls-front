import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { LocalStorageService } from '../../services/local-storage.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  isLogin: boolean = false;
  constructor(
    private http: HttpClient,
    private localStorage: LocalStorageService
  ) {}
  // Post method

  postRequest(url: string, body: {}): Observable<any> {
    return this.http.post<any>(environment.baseUrl + url, body);
  }
  Login(body: {}) {
    this.postRequest('/api/Account/Login', body).subscribe({
      next: (res: any) => {
        console.log('res', res);
        this.localStorage.setLocal('token',res.data.token);
        if (res) {
          this.isLogin = true;
        }
      },
      error: (error: any) => {},
    });
  }
}
