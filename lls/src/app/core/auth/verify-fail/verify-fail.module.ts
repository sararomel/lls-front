import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VerifyFailRoutingModule } from './verify-fail-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    VerifyFailRoutingModule
  ]
})
export class VerifyFailModule { }
