import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VerifyCheckRoutingModule } from './verify-check-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    VerifyCheckRoutingModule
  ]
})
export class VerifyCheckModule { }
